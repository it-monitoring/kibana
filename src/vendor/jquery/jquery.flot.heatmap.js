"use strict";

// Author: Lukas Havemannn
// PPI AG 2014

 /*global jQuery:false */

(function($) {
  var model, target;
  var models = {};
  function init(plot) {
    plot.hooks.draw.push( function(plot, newCtx) {
      newCtx.clearRect(0,0,newCtx.canvas.style.width, newCtx.canvas.style.height)
      var options = plot.getOptions();
      if(options.series.heatmap.show){
        target = $(plot.getCanvas()).parent();
        draw(plot, newCtx, $.plot.heatmap.coloring);
      }
    });
    plot.hooks.bindEvents.push(function(plot, eventHolder) {
      var options = plot.getOptions();
        if (options.series.heatmap.show && options.series.heatmap.tooltip) {
          eventHolder.unbind("mousemove").mousemove(onMouseMove);
        }
    });

    plot.hooks.bindEvents.push(function(plot, eventHolder) {
      var options = plot.getOptions();
      eventHolder.unbind("click")
      if (options.have_urls){
          eventHolder.click(onMouseClick);
      }
    });

    function draw(plot, newCtx, coloring) {
      var options = plot.getOptions();
      var opts = options.series.heatmap;
      var canvasWidth  = plot.getPlaceholder().width();
      var tileWidth = options.tileWidth;
      var tileHeight = options.tileHeight;
      var ctx = newCtx;
      var heatmap_start_point = options.entity_width + options.entity_gap

      model = buildHeatmapModel(plot.getData(), tileWidth, tileHeight, heatmap_start_point, opts.id);
      if (options.legend.left_show) renderValueLegend();
      renderAxisLegend(options.entity_width, heatmap_start_point, opts.id);
      renderTiles(model, coloring);

      // render functions
      function renderTiles(model, coloring){
        $.each(model.tiles, function(idx, tile){
          var x = opts.legend.leftOffset + tile.x;
          var y = tile.y;
          ctx.fillStyle = tile['color'] = coloring(tile.heat);
          ctx.fillRect(x, y, model.tile.width,
                             model.tile.height - options.tile.padding);
        });
      }

      function renderValueLegend() {

        var legendTileHeight = options.legend.legendHeight;
        var legendTileWidth = options.legend.legendWidth;

        var maxValueWidth = ctx.measureText(Math.round(model.range.max)).width;
        var base_y = tileHeight
        var x =  calculateLegendPosition(canvasWidth, tileWidth, maxValueWidth) + options.entity_width
        // render colored tile
        ctx.fillStyle = legend_colours[0];
        ctx.fillRect (x, base_y, legendTileWidth, legendTileHeight);

        // render value
        ctx.fillStyle = opts.fontColor;
        ctx.fillText("No data", x + legendTileWidth + options.tile.padding * 2, base_y + (legendTileHeight / 3) * 2);

        for(var i = 1; i <legend_keys.length; i++) {
          var value = legend_keys[i]
          var color = legend_colours[value];

          var y = base_y + (i * (legendTileHeight + options.tile.padding));

          // render colored tile
          ctx.fillStyle = color;
          ctx.fillRect (x, y, legendTileWidth, legendTileHeight);

          ctx.fillStyle = opts.fontColor;
          ctx.fillText("<="+value, x + legendTileWidth + options.tile.padding * 2, y + (legendTileHeight / 3) * 2, 50);
        }
      }

      function renderAxisLegend(entity_width, heatmap_start, heatmap_id) {
        var names = [];
        var start = tileHeight * 0.75
        $.each(options.x_values, function(idx, value){
          var x = opts.legend.leftOffset;
          //var y = idx * ((tileHeight - 1) +  options.tile.padding) + tileHeight * 1.5 + opts.legend.padding / 2;
          var y =  start + ((idx+ 1) * tileHeight)
          // for onmouse click..
          var name = {};
          name.value = value;
          name.x = x;
          name.y = y;
          name.length = value.length;
          name.width = entity_width;
          name.font_size = 12;
          names.push(name);
          if (value.length > options.name_length) value = value.substring(0, options.name_length) +"..";
          renderText(value, x, y, "left", entity_width);
        });
        models[heatmap_id]["names"]= names;

        $.each(options.y_values, function(idx, value){
          var value = value < 10 ? "0" + value+ " ":value+" "//+ "h";
          var x = (opts.legend.leftOffset  + tileWidth * idx  + (tileWidth / 2) ) + heatmap_start;
          var y = start;

          renderText(value, x, y, "center", tileWidth, 10);
          idx ++;
        });
      }

      function renderText(text, x, y, align, width, size) {
        var width = (width) ? width : 250
        var size = (size) ? size : 12
        ctx.textAlign = (align) ? align : "left";
        ctx.fillStyle = opts.fontColor;
        ctx.font = size+'px monospace';
        ctx.fillText(text, x, y, width);
      }


      function calculateLegendPosition(canvasWidth, tileWidth, maxValueWidth) {
        var rightX = opts.legend.leftOffset + options.tile.x * tileWidth + 30;
        var legendOffset = Math.min(opts.legend.rightOffset, maxValueWidth);

        var x = canvasWidth - legendOffset;
        if(x > rightX) x = rightX;

        return x;
      }
    } // END draw

    function buildHeatmapModel(data, tileWidth, tileHeight, plotOff, heatmapId) {
      var model = {
        tile : {
          width : tileWidth,
          height: tileHeight,
        },
        tiles : [],
        range : getValueRange(data),
      };
      $.each(data, function(idx, val){

        var tile = {
          value     : val.data[0][1],
          label     : val.label,
        };

        tile['heat'] = tile.value;
        tile.hour = val.data[0][0]
        tile.y = val.data[0][2] * tileHeight;
        tile.x = (val.data[0][0] * tileWidth) + plotOff;
        model.tiles.push(tile);
      });
      models[heatmapId] = {}
      models[heatmapId]["tiles"] = model.tiles
      models[heatmapId]["tile"] = model.tile

      function collided(id, x, y){
        var all_tiles = models[id]["tiles"]
        var width = models[id]["tile"].width
        var height = models[id]["tile"].height
        for(var idx in all_tiles) {
          var tile = all_tiles[idx];
          if(x > tile.x && x < tile.x + width) {
            if(y > tile.y && y < tile.y + height) {
              return tile;
            }
          }
        }
        return false;
      }

      model.collided = collided;
      return model;
    }

    function onMouseMove(event) {
      var options = plot.getOptions();
      var opts    = options.series.heatmap;

      var offset  = plot.offset();
      var canvasX = parseInt(event.pageX - offset.left, 10);
      var canvasY = parseInt(event.pageY - offset.top, 10);
      var elements_under = document.querySelectorAll( ":hover" );
      var id;
      for(var idx in elements_under){
          var elem = elements_under[idx]
          if (typeof elem == 'object' && elem.getAttribute('name') == "heatmap"){
              id = elem.id
          }
      }
      var tile = model.collided(id, canvasX - opts.legend.leftOffset, canvasY, 0);
      var pos = { pageX: event.pageX, pageY: event.pageY };
      target.trigger("plothover", [pos, tile]);
    }

    // Possible helper function for onMouseClick
    function collided_names(x, y, id){
      var all = models[id]["names"]
      for(var idx in all) {
        var tile = all[idx];
        if ((tile.y - tile.font_size) <= y && y <= tile.y){
          if (tile.x <= x && x <= tile.width){
            return tile.value
          }
        }
      }
      return false;
    }
    function onMouseClick(event) {
      /*
       *Now working!
       *
       */
      var options = plot.getOptions();
      var opts    = options.series.heatmap;

      var offset  = plot.offset();
      var canvasX = parseInt(event.pageX - offset.left, 10);
      var canvasY = parseInt(event.pageY - offset.top, 10);
      var elements_under = document.querySelectorAll( ":hover" );
      var id;
      for(var idx in elements_under){
          var elem = elements_under[idx]
          if (typeof elem == 'object' && elem.getAttribute('name') == "heatmap"){
              id = elem.id
          }
      }
      var tile = collided_names(canvasX - opts.legend.leftOffset, canvasY, id);
      var pos = { pageX: event.pageX, pageY: event.pageY };
      if (tile != false) {
          target.trigger("plotclick", [pos, tile, id]);
      }
    }

  } // END init


  function getValueRange(data) {
    var min = 0,
        max = 0;

    $.each(data, function(idx, value) {
      var val = value.data[0][1];

      if(val < min){
        min = val;
      } else if(val > max) {
        max = val;
      }
    });

    return {
      "min" : min,
      "max" : max,
      "diff" : max - min
    };
  }

  var legend_colours = {0:"#e5e5e5", 40 : "#E24D42", 70: " #EAB839", 100:"#7EB26D"}
  var legend_keys = [0, 40, 70, 100]

  function getStandardColor(heat){
    for (var x=0; x < legend_keys.length; x++){
      var key = legend_keys[x];
      if (heat <= key){
        return legend_colours[key];
      }
    }
  }

  // make some functions public - is this the right way TODO?
  !$.plot.heatmap && ($.plot.heatmap = {});

  $.plot.heatmap.coloring             = getStandardColor;
  $.plot.heatmap.getValueRange        = getValueRange;

  var options = {
    series: {
      heatmap: {
        show: false,
        fontColor: "#333333",
        legend : {
          padding: 10, // padding for entities from top of panel
          leftOffset: 0, // the offset of everything from left hand side
          rightOffset: 120
        }
      }
    }
  };

  $.plot.plugins.push({
    init: init,
    options: options,
    name: "heatmap",
    version: "1.0"
  });

})(jQuery);
