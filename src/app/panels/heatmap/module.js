// Author: Lukas Havemann
// PPI AG 2014
//require.config({
//  paths : {
//    'jquery.flot.heatmap' : '../app/panels/heatmap/jquery.flot.heatmap',
//  },
//
//  shim : {
//    'jquery.flot.heatmap': ['jquery', 'jquery.flot'],
//  }
//});

define([
  'angular',
  'app',
  'lodash',
  'jquery',
  'kbn',
  'jquery.flot',
  'jquery.flot.heatmap',
],
function (angular, app, _, $, kbn) {
  "use strict";

  var $tooltip = $('<div>');
  var module = angular.module('kibana.panels.heatmap', []);
  app.useModule(module);
  module.controller('heatmap', function($scope, querySrv, dashboard, filterSrv, fields) {
    $scope.panelMeta = {
      modals : [
        {
          description: "Inspect",
          icon: "icon-info-sign",
          partial: "app/partials/inspector.html",
          show: $scope.panel.spyable
        }
      ],

      editorTabs : [
        {title:'Queries', src:'app/partials/querySelect.html'}
      ],
      status  : "Stable",
      description : "Displays the results of an elasticsearch facet as a heatmap"
    };

    // Set and populate defaults
    var _d = {
      /** @scratch /panels/heatmap/5
       * === Parameters
       *
       * timestamp:: The field where the timestamp is
       */
      timestamp : '@timestamp',
      key : '@fields.entity',
      min_field: '@message.service_status',
      metric_id: '13107',
      metric_field: '@fields.metric_id',
      entities : '',
      span: 6,
      group : false,
      tstat : 'sls',
      show : true,
      have_urls : true,
      ignore_metric_id: false,
      /** @scratch /panels/heatmap/5
       * spyable:: Set spyable to false to disable the inspect button
       */
      spyable : true,

      /** @scratch /panels/heatmap/5
       *
       * ==== Queries
       * queries object:: This object describes the queries to use on this panel.
       * queries.mode::: Of the queries available, which to use. Options: +all, pinned, unpinned, selected+
       * queries.ids::: In +selected+ mode, which query ids are selected.
       */
      queries     : {
        mode        : 'all',
        ids         : []
      },
    };
    _.defaults($scope.panel,_d);

    $scope.init = function () {
      $scope.$on('refresh', function(){
        $scope.get_data();
      });
      $scope.get_data();
    };

    $scope.get_data = function() {
      // Make sure we have everything for the request to complete
      if(dashboard.indices.length === 0) {
        return;
      }
      $scope.panelMeta.loading = true;
      var request,
        boolQuery,
        queries;
      $scope.field = _.contains(fields.list, $scope.panel.field + '.raw') ?
        $scope.panel.field + '.raw' : $scope.panel.field;

      request = $scope.ejs.Request().indices(dashboard.indices);
      $scope.panel.queries.ids = querySrv.idsByMode($scope.panel.queries);
      queries = querySrv.getQueryObjs($scope.panel.queries.ids);

      var mybool = filterSrv.getBoolFilter(filterSrv.ids());

      if($scope.panel.ignore_metric_id !== true) {
        mybool.must($scope.ejs.TermFilter($scope.panel.metric_field, $scope.panel.metric_id));
      }

      mybool.must($scope.ejs.TermFilter($scope.panel.metric_field, $scope.panel.metric_id));
      var dateAgg = getDateAgg();

      $scope.results = [];
      $scope.entity_list = [];
      $scope.entity_result = {};
      $scope.y_positions = {};
      $scope.name_to_query = {};
      $scope.agg = $scope.ejs.MinAggregation('type');
      $scope.string_percent = {
        "available":100,
        "degraded":70,
        "unavailable":1,
        "No data": -1
      };
      $scope.string_names = Object.keys($scope.string_percent);
      $scope.sls = false;
      if ($scope.panel.tstat === 'avg') {
        $scope.agg = $scope.ejs.AvgAggregation('type');
      }
      else if ($scope.panel.tstat === 'max') {
        $scope.agg = $scope.ejs.MaxAggregation('type');
      }
      else if ($scope.panel.tstat === 'sls') {
        $scope.agg = $scope.ejs.TermsAggregation('type');
        $scope.sls = true;
        $scope.panel.show = false;
      }

      if ($scope.panel.group){
        queries.sort(function(a, b) {
          var a_name = (a.alias) ? a.alias : a.query;
          var b_name = (b.alias) ? b.alias : b.query;
          return (b_name.toLowerCase()) < (a_name.toLowerCase());
        });
        var num_results = $scope.panel.entities.length > 0 ? queries.length + 1 : queries.length;
        _.each(queries, function(q, idx){
          boolQuery = $scope.ejs.BoolQuery();
          boolQuery = boolQuery.should(querySrv.toEjsObj(q));
          var entity = (q.alias) ? q.alias : q.query;
          $scope.name_to_query[entity] = q.query;
          $scope.entity_list.push(entity);
          $scope.y_positions[entity] = idx +1;
          doRequest(true, dateAgg, mybool, request, boolQuery, num_results, entity);
        });
        var others = $scope.panel.entities;
        if (others.length > 0){
          $scope.entity_list.push(others);
          $scope.y_positions[others] = queries.length + 1;
          getQuery(mybool, false);
          doRequest(true, dateAgg, mybool, request, $scope.ejs.BoolQuery(), num_results, others);
        }
      }
      else{
        boolQuery = $scope.ejs.BoolQuery();
        _.each(queries,function(q) {
          boolQuery = boolQuery.should(querySrv.toEjsObj(q));
        });
        getQuery(mybool, true);
        doRequest(false, dateAgg, mybool, request, boolQuery, 0, null);
      }
    };

    $scope.set_refresh = function (state) {
      $scope.refresh = state;
    };

    $scope.close_edit = function() {
      $scope.get_data();
      $scope.$emit('render');
    };

    function getDateAgg(){
      var format = "yyyy-MM-dd";
      var tdate  = new Date();
      var to = tdate.getTime();
      var lasthour = tdate.getHours();
      $scope.hourlist= [];
      var start = lasthour;
      $scope.hours = 24;
      // get the y-axis labels
      while (start < $scope.hours){
        $scope.hourlist.push(start);
        start++;
      }
      start = 0;
      while (start < lasthour){
        $scope.hourlist.push(start);
        start++;

      }
      var onehour = 3600000; // in miliseconds
      var dateRangeAgg = $scope.ejs.DateRangeAggregation('dates')
                              .field($scope.panel.timestamp).format(format);

      // Need to get the times for the last 24 hours..
      var hours = $scope.hours;
      while (hours > 0){
        var from = to - onehour;
        dateRangeAgg.range(from.toString(), to.toString(), hours.toString());
        to = from;
        hours --;
      }
      return dateRangeAgg;
    }
    function getQuery(boolFilter, push_names){
      // Get any entity names given save add them to the bool filter.
      var split = $scope.panel.entities.split(',');
      for (var x in split){
        var name = split[x].trim();
        if (name.length > 0){
          if (push_names){
            $scope.entity_list.push(name);
            $scope.y_positions[name] = $scope.entity_list.length;
          }
          boolFilter.should($scope.ejs.TermFilter($scope.panel.key, name));
        }
      }
    }
    function doRequest(grouped, dateRangeAgg, mybool, request, boolQuery, leng, entity){
      // Our aggregation request:
      // Date Range Aggregation to get the results for each hour
      // Terms aggregation to aggregate based on entity name
      // Avg aggregation to aggregate the availability.
      if (grouped){
        request = request
          .agg(dateRangeAgg.agg(
                $scope.agg.field($scope.panel.min_field)))
          .query($scope.ejs.FilteredQuery(boolQuery, mybool)).size(0);
      }
      else {
        request = request
          .agg(dateRangeAgg.agg(
              $scope.ejs.TermsAggregation('terms').size(0).order("_term", "asc").field($scope.panel.key).agg(
                  $scope.agg.field($scope.panel.min_field))))
          .query($scope.ejs.FilteredQuery(boolQuery, mybool)).size(0);
      }

      // Populate the inspector panel
      $scope.inspector = angular.toJson(JSON.parse(request.toString()), true);

      var results = request.doSearch();
      // Populate scope when we have results
      results.then(function(results) {
        $scope.panelMeta.loading = false;
        if (!grouped){
          $scope.result = results;
        }
        else{
          $scope.results.push(results);
          $scope.entity_result[entity] = results;
        }
        if (!grouped || $scope.results.length === leng){
          $scope.$emit('render');
        }
      });
    }
  });
  var all_things = {};
  module.directive('heatmap', function(querySrv) {
    return {
      restrict: 'A',
      link: function(scope, main_elem) {
        var plot;
        var id = "heatmap-"+ document.getElementsByName('heatmap').length;
        var our_elem = $("<div name='heatmap' id='"+id+"'>");
        all_things[id] = scope.name_to_query;
        main_elem.append(our_elem);

        // Receive render events
        scope.$on('render',function(){
          render_panel();
        });
        var time = new Date();
        our_elem.bind("plothover", function (event, pos, tile) {
          if (tile) {
            var yesterday = time.getHours();
            var h = yesterday + tile.hour;
            var exact = h > 23? h-23: h;
            var min = time.getMinutes();
            min = min < 10? "0"+min : min;
            var value = tile.value;
            if (scope.sls){
              value = _.invert(scope.string_percent)[tile.value] || tile.value;
            }
            $tooltip
              .html(
                kbn.query_color_dot(tile.color, 20) + ' ' +
                tile.label + " (" + value +") "+ (exact -1 ) + ':' + min + ' - ' + exact + ':' + min
              )
            .place_tt(pos.pageX, pos.pageY);
          } else {
            $tooltip.remove();
          }
        });
        our_elem.bind("plotclick", function (event, pos, entity, id) {
          var url = 'https://meter.cern.ch/public/_plugin/kibana/#/dashboard/elasticsearch/Metrics:%20Availability?query=';
          var query = all_things[id][entity];
          if (query !== undefined){
            // Grouped query
            url = url + query;
          } else {
            url = url + "@fields.entity:%20" + entity;
          }
          window.open(url, '_blank');
        });

        function build_results() {
          /* Get the results.
           * Loop over the buckets from the aggregation
           * as we do, keep track of the entity names we encounter
           * If we encounter a new one, add data for previous timestamps.
          * */
          all_things[id] = scope.name_to_query;
          scope.data = [];
          var x_positions = []; // only needed if we find a new element half way through the list o
          var longest_entity = 0;
          var name;
          for (var x in scope.entity_list){
            name = scope.entity_list[x];
            if (name.length > longest_entity) {
              longest_entity = name.length;
            }
          }
          if (scope.panel.group){
            for (name in scope.entity_result){
              var results = scope.entity_result[name];
              if (name.length > longest_entity) {
                longest_entity = name.length;
              }

              /*jshint -W083 */
              _.each(results.aggregations.dates.buckets, function (v){
                var x_pos = parseInt(v.key, 10) - 1;
                var the_val = findvalue(v.type);
                var y_pos = scope.y_positions[name];
                scope.data.push(createSlot(name, x_pos, the_val, y_pos));
              });
            }
          }
          else{
            if (typeof scope.result.aggregations === "undefined") {
              return;
            }
            _.each(scope.result.aggregations.dates.buckets, function(v) {
              var x_pos = parseInt(v.key, 10) - 1;
              var done = [];
              _.each(v.terms.buckets, function(ter){
                var entity = ter.key;
                if (entity.length > longest_entity) {
                  longest_entity = entity.length;
                }
                done.push(entity);

                var y_pos = scope.y_positions[entity];
                if (y_pos === undefined){
                  // we havent seen this entity/row before,and ... add it and add in the ones that didn't exist
                  scope.entity_list.push(entity);
                  y_pos = scope.entity_list.length;
                  scope.y_positions[entity] = y_pos;
                  // now add missing data
                  for (var x in x_positions) {
                    scope.data.push(createSlot(entity, x_positions[x], -1, y_pos));
                  }
                }
                scope.data.push(createSlot(entity, x_pos, findvalue(ter.type), y_pos));
              });

              x_positions.push(x_pos);
              // Now check to see if any names given were not found
              for (var each in scope.entity_list){
                var name = scope.entity_list[each];
                if (done.indexOf(name) > -1) {
                  continue;
                }
                scope.data.push(createSlot(name, x_pos, -1, scope.y_positions[name]));
              }
            });
          }
          scope.longest_entity = longest_entity;
        }
        function createSlot(the_label, x_position, the_value, y_position){
          /* Returns a dictionary containing the data for a tile on the heatmap
           *
           * the_label:  A string for the label of the tile when hovering over with the mouse
           * x_position: Where on the x-axis the tile should be placed
           * the_value:  The value for the tile
           * y_position: Where on the y-axis the tile shold be placed
           *
           * */
          return {label: the_label, data:[[x_position, the_value, y_position]], actions:true};
        }
        function findvalue(type){
          /* Returns the value (a number between -1 and 100) of the result
           * */
          if (scope.sls){
            var values = [];
            _.each(type.buckets, function(dicts){
              values.push(dicts.key);
            });
            var lowest;
            _.each(scope.string_names, function(name){
              if (values.indexOf(name) > -1 && (typeof lowest === "undefined"|| scope.string_percent[name] < lowest)) {
                lowest = scope.string_percent[name];
              }
            });
            return lowest || -1;
          }
          return type.value == null ? -1 : type.value.toPrecision(3);


        }
        // Function for rendering panel
        function render_panel() {
          var cursor = scope.panel.have_urls ? 'pointer' : 'default';
          our_elem.css('cursor', cursor);

          var chartData;
          build_results();
          // Make a clone we can operate on.
          chartData = _.clone(scope.data);
          chartData = _.without(chartData, _.findWhere(chartData,{meta:'missing'}));
          chartData = _.without(chartData, _.findWhere(chartData,{meta:'other'}));

          var longest_allowed = 32; // longest allowed entity name

          // calculate the canvas width
          var window_width = $(window).width();
          var window_span = 12 + 1; // window is divided into 12 spans + one span for menus at side
          var span_width = Math.floor(window_width / window_span); // width of each span
          var full_width = span_width * scope.panel.span; // width of this panel

          var entity_width = (full_width/3 > 250)? 250: Math.floor(full_width/3); // width for entity names.
          var entity_gap = 10; // the width of the gap between entity names and the first heatmap tiles
          var legend_width = scope.panel.show ? full_width / 8 : 0;// The max width of the legend (approx.)

          var width_left = full_width - entity_width - entity_gap - legend_width; // whats left for the tiles
          var tileWidth = Math.floor(width_left/ scope.hours); // width of heatmap tiles.
          var tileHeight = (tileWidth < 19) ? tileWidth : 19; // height of heatmap tiles
          var legendHeight = tileHeight; // height of legend tiles
          var legendWidth = (tileWidth > 30)? 30 : tileWidth; // width of legend tiles

          var legend_steps = 5;
          var tile_padding = 1;
          // calculate the height of the canvas
          var full_legend_height  = scope.panel.show ? ((legend_steps +1) * legendHeight) + legend_steps : 0;
          // +1 - room for top axis, and legend_steps for padding

          var height = ((scope.data.length / scope.hours)+ 1.5) * (tileHeight); // +1 for the time legend
          if (height < full_legend_height) {
            height = full_legend_height;
          }
          our_elem.css({height:height, width: full_width});
          plot = $.plot(our_elem, chartData, {
            legend: { show: false,
                      left_show : scope.panel.show,
                      legendHeight: legendHeight, // legend tile height
                      legendWidth: legendWidth    // legend tile width
                    },
            tile: {x: scope.hours, padding: tile_padding},
            series: {
              heatmap: {
                show: true,                       // show the heatmap
                tooltip : true,                   // show the tooltips (when you hover over with mouse)
                id : id                           // the id of this heatmap div element
              }
            },
            entity_gap: 10,                      // size of padding between entity names and start of heatmap tiles
            have_urls: scope.panel.have_urls,
            tileWidth: tileWidth,                // width of heatmap tiles
            tileHeight: tileHeight,              // height of heatmap tiles
            entity_width: entity_width,          // the width of the entities
            name_length: longest_allowed,        // the max length an entity name should be
            grid: false,
            colors: querySrv.colors,
            x_values : scope.entity_list,        // list of entity names (axis names)
            y_values : scope.hourlist            // list of times  (axis names)
          });

        }
      }
    };
  });
});
