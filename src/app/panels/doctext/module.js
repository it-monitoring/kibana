/** @scratch /panels/5
 *
 * include::panels/doctext.asciidoc[]
 */

/** @scratch /panels/doctext/0
 * == text
 * Status: *Stable*
 *
 * The doctext panel is used for displaying the last values of text or html fields
 *
 */
define([
  'angular',
  'app',
  'lodash',
  'jquery',
  'kbn',
  'config',
  'chromath',
  'numeral'
],
function (angular, app, _) {
  'use strict';
  var module = angular.module('kibana.panels.doctext', []);
  app.useModule(module);

  module.controller('doctext', function($scope, $rootScope, querySrv, dashboard, filterSrv) {
    $scope.panelMeta = {
      editorTabs : [
        {title:'Queries', src:'app/partials/querySelect.html'}
      ],
      modals : [
        {
          description: "Inspect",
          icon: "icon-info-sign",
          partial: "app/partials/inspector.html",
          show: $scope.panel.spyable
        }
      ],
      status  : "Stable",
      description : "A text panel that can show plain text or HTML."
    };
    // Set and populate defaults
    var _d = {
      field: "@message.availability_description",
      show_field_name: true,
      unique_entity: true,
      entity_field : "@fields.entity",
      errors: false,
      style: {},
      spyable : true,
      queries: {mode: 'all', ids: []},
    };
    _.defaults($scope.panel,_d);

    $scope.init = function() {
      $scope.$on('refresh',function(){$scope.get_data();});
      $scope.get_data();
      $scope.ready = false;
    };
    $scope.close_edit = function() {
      $scope.get_data();
    };

    $scope.get_data = function(){
      // Make sure we have everything for the request to complete
      if(dashboard.indices.length === 0){
        return;
      }
      $scope.panelMeta.loading = true;
      var request = $scope.ejs.Request().indices(dashboard.indices);
      $scope.panel.queries.ids = querySrv.idsByMode($scope.panel.queries);
      var queries = querySrv.getQueryObjs($scope.panel.queries.ids);
      var boolQuery = $scope.ejs.BoolQuery();
      _.each(queries,function(q) {
        boolQuery = boolQuery.should(querySrv.toEjsObj(q));
      });
      $scope.fields = [];
      var filters = [];
      var split = $scope.panel.field.split(",");
      for (var x in split){
        var name = split[x].trim();
        if (name.length > 0){
          $scope.fields.push(name);
          filters.push($scope.ejs.ExistsFilter(name));
        }
      }
      var fields_exist = $scope.ejs.OrFilter(filters);
      request = $scope.ejs.Request().indices(dashboard.indices)
          .query($scope.ejs.FilteredQuery(boolQuery, filterSrv.getBoolFilter(filterSrv.ids())))
          .fields($scope.fields)
          .filter(fields_exist)
          .sort($scope.ejs.Sort('@timestamp').ignoreUnmapped(true).desc())
          .size(1).agg($scope.ejs.CardinalityAggregation("how_many").field($scope.panel.entity_field));
      $scope.inspector = angular.toJson(JSON.parse(request.toString()),true);
      var results = request.doSearch();
      results.then(function(results){
        $scope.panelMeta.loading = false;
        $scope.results = results;
        $scope.$emit('render');
      });
    };
  });

  module.directive('doctext', function() {
    return {
      restrict: 'A',
      link: function(scope) {
        scope.$on('render', function() {
          render_panel();
        });

        function render_panel() {
          function build_results(){
            scope.panel.content = "";
            var count;
            if (typeof scope.results === 'undefined') {
              count = 0;
            }
            else {
              count = scope.results.aggregations.how_many.value;
            }

            var correct_amount = true;
            if (scope.panel.unique_entity && count !== 1){
              correct_amount = false;
            }
            var missing = [];
            if (correct_amount && scope.results.hits.hits.length > 0){
              // assume we have found all fields
              var hits = scope.results.hits.hits[0];
              for (var indx in scope.fields){
                var field_name = scope.fields[indx];
                if (!(field_name in hits.fields)){
                  missing.push(field_name);
                  continue;
                }
                var desc = hits.fields[field_name][0].trim();
                // only turn things into links where were are sure it's only a link
                if (desc.indexOf("http") === 0 && !(/\s/.test(desc)) && desc.indexOf("<br/>") === -1){
                  desc = '<a href="'+desc+'">'+ desc + '</a>';
                }
                if (indx > 0) {
                  scope.panel.content += "<br/>";
                }
                if (scope.panel.show_field_name) {
                  field_name = field_name.split('.');
                  scope.panel.content += "<strong>" + field_name[field_name.length-1] + ":</strong><br/>";
                }
                desc = desc.replace(/< /g, "&lt; ");
                desc = desc.replace(/ > /g, " &gt;");
                scope.panel.content += desc + "<br/>";
              }
            }
            else if (!correct_amount && scope.panel.errors) {
              scope.panel.content += "<br/>Please select a single entity to see the associated information.";
            }
            else if (scope.results.hits.hits.length === 0 && scope.panel.errors) {
              scope.panel.content += "<br/>No documents found matching any of those fields.";
            }
            if (scope.panel.errors && missing.length > 0){
              scope.panel.content += "<br/>Did not find the following fields in the latest document: " + missing;
            }
          }
          build_results();
          if(!scope.$$phase) {
            scope.$apply();
          }
        }
      }
    };
  });
});
