/** @scratch /panels/5
 *
 */

/** @scratch /panels/square/0
 *
 * == square
 *
 * A panel which shows square for each entity recovered and change the colours based on their service_status
 *
 */
define([
  'angular',
  'app',
  'lodash'
],
function (angular, app, _) {
  'use strict';

  var module = angular.module('kibana.panels.square', []);
  app.useModule(module);

  module.controller('square', function($scope, kbnIndex, querySrv, dashboard, filterSrv) {

    $scope.panelMeta = {
      modals : [
        {
          description: "Inspect",
          icon: "icon-info-sign",
          partial: "app/partials/inspector.html",
          show: $scope.panel.spyable
        }
      ],
      editorTabs : [
        {title:'Queries', src:'app/partials/querySelect.html'}
      ],
      status  : "Beta",
      description : "A panel for showing service availabilities in a similar fashion to the SNOW page. "+
      "i.e. in squares"
    };

    // Set and populate defaults
    var _d = {
      /** @scratch /panels/square/5
       *
       * === Parameters
      /** @scratch /panels/square/5
       * entity_field:: any valid elasticsearch field name
       */
      entity_field : '@fields.entity',
      /** @scratch /panels/square/5
       * service_status:: any valid elasticsearch field which contains the service status
       */
      service_status : '@message.service_status',
      /** @scratch /panels/square/5
       * entities:: A comma separated list of entities you always want to see.
       */
      entities : '',
      /** @scratch /panels/trends/5
       * spyable:: Set to false to disable the inspect icon
       */
      spyable: true,
      /** @scratch /panels/trends/5
       *
       * ==== Queries
       * queries object:: This object describes the queries to use on this panel.
       * queries.mode::: Of the queries available, which to use. Options: +all, pinned, unpinned, selected+
       * queries.ids::: In +selected+ mode, which query ids are selected.
       */
      queries     : {
        mode        : 'all',
        ids         : []
      },
      style   : { "font-size": '14pt'},
    };
    _.defaults($scope.panel,_d);

    $scope.init = function () {
      $scope.$on('refresh', function(){$scope.get_data();});
      $scope.get_data();
    };

    $scope.get_data = function() {

      // Make sure we have everything for the request to complete
      if(dashboard.indices.length === 0) {
        return;
      }

      $scope.panelMeta.loading = true;

      var request = $scope.ejs.Request().indices(dashboard.indices);
      // Make sure we have the existing queries to add to the request
      $scope.panel.queries.ids = querySrv.idsByMode($scope.panel.queries);
      var queries = querySrv.getQueryObjs($scope.panel.queries.ids);
      var boolQuery = $scope.ejs.BoolQuery();
      _.each(queries, function(q) {
        boolQuery = boolQuery.should(querySrv.toEjsObj(q));
      });

      // build the full query, remembering to add the queries and filters
      // our query:
      // in the timeframe get uniq entities and get last document for that
      request = request
          .agg($scope.ejs.TermsAggregation('terms')
              .field($scope.panel.entity_field)
              .size(0)
              .order('_term','asc')
              .agg($scope.ejs.TopHitsAggregation('getlatest')
                  .sort([{'@timestamp': {'order' : 'desc'}}])
                  .source($scope.panel.service_status)
                  )
          )
          .fields($scope.panel.service_status)
          .query($scope.ejs.FilteredQuery(boolQuery, filterSrv.getBoolFilter(filterSrv.ids())));

      // Populate the inspector panel
      $scope.inspector = angular.toJson(JSON.parse(request.toString()),true);
      // do the search

      // once we have the results do something
      //results.then..
      process_results(request.doSearch());
    };

    // Populate scope when we have results
    var process_results = function(results) {
      results.then(function(results) {
        $scope.panelMeta.loading = false;
        var colours = {"available":"#7EB26D",
                       "degraded": "#EAB839",
                       "unavailable": "#E24D42"};
        $scope.data = [];
        var named_entities = [];
        var split =  $scope.panel.entities.split(',');
        for (var x in split){
          var name = split[x].trim();
          if (name.length > 0){
            named_entities.push(name);
          }
        }
        // loop around the results populate data
        try {
            _.each(results.aggregations.terms.buckets, function(x, index){
                var the_status = x.getlatest.hits.hits[0]._source["@message"]["service_status"];
                var name = x.key;
                if (named_entities.indexOf(name) > 1){
                  named_entities.splice(named_entities.indexOf(name), 1);
                }

                $scope.data[index] = {
                    name: x.key,
                    color: colours[the_status]
                  };
              });
          }
        catch(err){
            console.log("ERROR:");
            console.log(err);
          }
        // after populating the data go over the scope.panel.entities to see if we have found them
        // if not, add them but with no colour.
        var index = $scope.data.length;
        for (x in named_entities) {
          $scope.data[index] = {
            name: named_entities[x]
          };
          index += 1;
        }
        $scope.$emit('render');
      });
    };

    $scope.set_refresh = function (state) {
      $scope.refresh = state;
    };

    $scope.close_edit = function() {
      if($scope.refresh) {
        $scope.get_data();
      }
      $scope.refresh =  false;
      $scope.$emit('render');
    };

  });
});
