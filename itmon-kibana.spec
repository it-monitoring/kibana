Name: itmon-kibana
Version: 3.1.2
Release: 9%{?dist}
Summary: Customized Kibana 3 version for itmon-team
License: Apache
URL: https://git.cern.ch/web/itmon/kibana.git
Source: %{name}.tar.gz

%description
Customized Kibana 3 version for itmon-team

%prep
%setup -n %{name}

%build
cd build/
tar xvf kibana-latest.tar.gz

%install
%{__mkdir} -p %{buildroot}/var/www/%{name}

%{__cp} -r $RPM_BUILD_DIR/%{name}/build/kibana-latest/* %{buildroot}/var/www/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_var}/www/itmon-kibana

%config(noreplace) /var/www/itmon-kibana/index.html
%config(noreplace) /var/www/itmon-kibana/config.js
%config(noreplace) /var/www/itmon-kibana/favicon.ico
%config(noreplace) /var/www/itmon-kibana/app/dashboards/default.json

%changelog
* Thu Mar 31 2016 Luis Pigueiras - <luis.pigueiras@cern.ch> - 3.1.2-8
- OS-2723 Make heatmap configurable for data other than lemon metrics

* Tue Jan 12 2016 Luis Pigueiras - <luis.pigueiras@cern.ch> - 3.1.2-7
- AM-1969 Change link to heatmap to exact match

* Tue Dec 01 2015 Daniel Zolnai <daniel.zolnai@cern.ch> 3.1.2-6
- Revert code of last version

* Mon Nov 30 2015 Daniel Zolnai <daniel.zolnai@cern.ch> - 3.1.2-5
- INC0899524 Fix a bug with stacked lines without zerofill

* Mon Oct 26 2015 Susie Murphy<susie.murphy@cern.ch> - 3.1.2-4
- RQF0491688 Convert https links to actual links

* Wed Oct 21 2015 Susie Murphy<susie.murphy@cern.ch> - 3.1.2-3
- AM-1843 Add square panel

* Fri Sep 25 2015 Luis Pigueiras <luis.pigueiras@cern.ch> - 3.1.2-2
- Fix stacked (multifield)histogram panels

* Tue Sep 22 2015 Luis Pigueiras <luis.pigueiras@cern.ch> - 3.1.2-1
- Initial version of the Kibana package with itmon customizations
