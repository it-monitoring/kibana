# Build process

## Prequisites

```bash
$ yum install nodejs npm
```

## Before releasing a new version you should:

```bash
$ cd /path/to/kibana
$ npm install
$ grunt build
```

A new build will be created in `build/kibana-latest.tar.gz`. This file
will be used in the spec file in order to install.

After doing this you can proceed as usual, increasing the version number in
the spec file (only increase the release number, to keep track of the
real Kibana version we are using).

## Notes

**Do not run `grunt build` in AFS.** It will fail due to permission issues.
