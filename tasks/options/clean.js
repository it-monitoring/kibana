module.exports = function(config) {
  return {
    on_start: ['<%= destDir %>', '<%= tempDir %>', 'build/'],
    temp: ['<%= tempDir %>'],
    docs: ['<%= docsDir %>/kibana'],
  };
};
